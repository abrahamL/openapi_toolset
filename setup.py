from setuptools import setup, find_packages


setup(
    version='1.0.0',
    name='openapi_toolset',
    description='OPENAPI文档工具箱',
    author='Abraham',
    packages=find_packages(),
    include_package_data=True,
    install_requires=['flask', 'PyYAML'],
    scripts=['bin/openapi_to_html']
)
