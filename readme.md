# openapi_toolset

This is a tool to view and edit OPENAPI/Swagger document locally.

## Installing

use virtual environment(recommand)

```bash
$: virtualenv ~/py_env -p python3  # create a new virtual environment named py_env
$: soruce ~/py_env/bin/activate  # activate virtual enviroment
(py_env)$:pip install -e git+https://bitbucket.org/abrahamL/openapi_toolset#egg=openapi_toolset
```


## Quick Start

**view or edit document**

```bash
(py_env)$: cd demo
(py_env)$: ll
-rw-r--r-- 1 abraham abraham  759 9月   4 15:03 common.yaml
-rw-r--r-- 1 abraham abraham 1.1K 9月   4 17:14 main.yaml
-rw-r--r-- 1 abraham abraham  848 9月   4 16:37 students.yaml
-rw-r--r-- 1 abraham abraham  846 9月   4 16:46 teachers.yaml
(py_env)$: python -m openapi_toolset
 * Serving Flask app "openapi_toolset" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:9090/ (Press CTRL+C to quit)
```


**generate document html**

You can generate a html format document from source code with command `openapi_to_html`, 
the source code could be remote url or local file.

```bash
$: openapi_to_html https://generator.swagger.io/api/swagger.json --output_file=test.html

$: usage: openapi_to_html [-h] [--output_file OUTPUT_FILE] source_file

generate html format document from source code

positional arguments:
  source_file           url or file path of the source code, source code will
                        be parsed as yaml format unless path endswith .json

optional arguments:
  -h, --help            show this help message and exit
  --output_file OUTPUT_FILE
                        where to save the html generated from source code
```
