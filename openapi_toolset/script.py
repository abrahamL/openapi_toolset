import json
import os

from jinja2 import Template
import requests
import yaml

base_dir = os.path.dirname(os.path.abspath(__file__))


class InvalidYAML(Exception):
    pass


def get_document(_input):
    """根据_input获取文档内容，_input可以为URL或文件路径
    如果不是以.json结尾，都已yaml的格式进行解析
    """
    if _input.startswith('http'):
        r = requests.get(_input)
        if _input.endswith('.json'):
            return r.json()
        try:
            return yaml.load(r.text)
        except Exception as err:
            raise InvalidYAML(err)
    else:
        assert os.path.exists(_input), 'file %s not exists' % _input
        if _input.endswith('.json'):
            with open(_input) as f:
                return json.load(f)
        try:
            with open(_input) as f:
                return yaml.load(f.read())
        except Exception as err:
            raise InvalidYAML(err)


def generate_doc_file(_input, output_filename='api.html'):
    document = get_document(_input)
    template_file = os.path.join(base_dir,
                                 'templates/swagger_ui_index_cdn.html')
    with open(template_file) as f:
        text = f.read()
    template = Template(text)
    html = template.render(document=json.dumps(document))
    with open(output_filename, 'w') as f:
        f.write(html)
