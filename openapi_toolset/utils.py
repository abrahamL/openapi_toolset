import os
from urllib.parse import urljoin

from flask import abort, send_from_directory, current_app, request


def serve_file(base_path, filename):
    abspath = os.path.join(base_path, filename)
    if not filename or os.path.isdir(abspath):
        abort(403)
    return send_from_directory(base_path, filename, as_attachment=False)


def get_openapi_main_file_url():
    return urljoin(request.host_url,
                   '/workspace/%s' % current_app.config['OPENAPI_MAIN_FILE'])
