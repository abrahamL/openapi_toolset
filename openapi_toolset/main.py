from urllib.parse import unquote

from flask import Blueprint, current_app, render_template

from .decorators import no_cache, cors
from .utils import serve_file


bp = Blueprint('main', __name__, url_prefix='/')


@bp.route('', methods=('GET', ))
def index():
    return render_template('index.html')


@bp.route('/workspace/<filename>', methods=('GET', ))
@cors
@no_cache
def workspace_file(filename=''):
    filename = unquote(filename)
    return serve_file(current_app.config['WORKSPACE_DIR'], filename)
