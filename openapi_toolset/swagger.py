from urllib.parse import unquote

from flask import Blueprint, current_app, redirect, url_for, render_template

from .utils import serve_file, get_openapi_main_file_url

bp = Blueprint('swagger', __name__, url_prefix='/swagger')


@bp.route('/editor', methods=('GET', ))
def editor():
    return redirect(url_for('swagger.editor_index'))


@bp.route('/editor/index.html', methods=('GET', ))
def editor_index():
    api_file_url = get_openapi_main_file_url()
    return render_template(
        'swagger_editor_index.html', api_file_url=api_file_url)


@bp.route('/editor/<filename>', methods=('GET', ))
def editor_dict(filename=''):
    filename = unquote(filename)
    return serve_file(current_app.config['SWAGGER_EDITOR_DIR'], filename)


@bp.route('/ui', methods=('GET', ))
def ui():
    return redirect(url_for('swagger.ui_index'))


@bp.route('/ui/index.html', methods=('GET', ))
def ui_index():
    api_file_url = get_openapi_main_file_url()
    return render_template('swagger_ui_index.html', api_file_url=api_file_url)


@bp.route('ui/<filename>')
def ui_dist(filename=''):
    filename = unquote(filename)
    return serve_file(current_app.config['SWAGGER_UI_DIR'], filename)
