from flask import Flask


def create_app(config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(SECRET_KEY='dev', )
    if config is None:
        app.config.from_pyfile('config.py', silent=False)
    else:
        app.config.from_mapping(config)

    from .main import bp as main_bp
    app.register_blueprint(main_bp)
    from .swagger import bp as swagger_bp
    app.register_blueprint(swagger_bp)
    from .redoc import bp as redoc_bp
    app.register_blueprint(redoc_bp)

    return app
