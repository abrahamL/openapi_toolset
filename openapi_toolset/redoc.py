from urllib.parse import unquote

from flask import Blueprint, current_app, render_template, redirect, url_for

from .utils import serve_file, get_openapi_main_file_url

bp = Blueprint('redoc', __name__, url_prefix='/redoc')


@bp.route('', methods=('GET', ))
def redoc():
    return redirect(url_for('redoc.index'))


@bp.route('/index', methods=('GET', ))
def index():
    api_file_url = get_openapi_main_file_url()
    return render_template('redoc_index.html', api_file_url=api_file_url)


@bp.route('/<filename>', methods=('GET',))
def redoc_dist(filename=''):
    print(filename)
    filename = unquote(filename)
    return serve_file(current_app.config['REDOC_DIR'], filename)
