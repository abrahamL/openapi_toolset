from datetime import datetime
from functools import wraps


def no_cache(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        response = f(*args, **kwargs)
        response.headers['Last-Modified'] = datetime.now()
        response.headers['Cache-Control'] = \
            'no-store, no-cache, must-revalidate, pre-check=0, max-age=0'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = -1
        return response
    return decorated_function


def cors(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        response = f(*args, **kwargs)
        response.headers['Access-Control-Allow-Origin'] = '*'
        return response
    return decorated_function
