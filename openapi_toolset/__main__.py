import os

from . import create_app

if __name__ == '__main__':
    base_dir = os.path.dirname(os.path.abspath(__file__))
    OPENAPI_TOOLS_DIR = os.path.join(base_dir, 'openapi_tools')
    config = {
        'STATIC_DIR': os.path.join(base_dir, 'static'),
        'WORKSPACE_DIR': os.getcwd(),
        'SWAGGER_EDITOR_DIR': os.path.join(OPENAPI_TOOLS_DIR,
                                           'swagger_editor'),
        'SWAGGER_UI_DIR': os.path.join(OPENAPI_TOOLS_DIR, 'swagger_ui'),
        'REDOC_DIR': os.path.join(OPENAPI_TOOLS_DIR, 'ReDoc'),
        'OPENAPI_MAIN_FILE': 'main.yaml'
    }
    app = create_app(config=config)
    app.run(port=9090, threaded=True)
