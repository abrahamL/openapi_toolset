#!/bin/bash
set -e

cd ./swagger_resource
npm install

cp -R node_modules/swagger-editor-dist/ ../openapi_toolset/swagger_dist/editor
cp -R node_modules/swagger-ui-dist/ ../openapi_toolset/swagger_dist/ui

echo "Update Successfully"
